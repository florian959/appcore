<?php
/*                                        
   __ _   _ __    _ __     ___    ___    _ __    ___ 
  / _` | | '_ \  | '_ \   / __|  / _ \  | '__|  / _ \
 | (_| | | |_) | | |_) | | (__  | (_) | | |    |  __/
  \__,_| | .__/  | .__/   \___|  \___/  |_|     \___|
         | |     | |                                 
         |_|     |_|                                 
*/

Class application {
    
    public $path;
    public $request;
    
    public function __construct($path = False) {
        if(!$path) {
            exit("appcore: init: aucun dossier defini");
        }
        else{
            $this->path = $path;
        }
        if(isset($_SERVER['PATH_INFO'])) {
            $this->request = substr($_SERVER['PATH_INFO'], 1);   
        }
    }
    
    public function set($name, $value) {
        if(preg_match("#%PATH #", $value)) {
            $value = str_replace("%PATH ", $this->path, $value);
        }        
        DEFINE($name, $value);
    }
    
    public function home() {
        return $_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];
    }
    
    public function classLoad($file) {
        
        if(file_exists($this->path."/class/".$file.".class.php")) {
            include($this->path."/class/".$file.".class.php");
        }
        else {
            if(file_exists($this->path."/class/404.class.php")) {
                include($this->path."/class/404.class.php");
            }
        
            if(class_exists("notfound")) {
                $class = new notfound();
                $class->main();
            }
        }
        
        if(class_exists($file)) {
            $class = new $file();
            $class->main();
        }
        
    }
    
    public function viewLoad($file) {
        if(file_exists("../views/".$file.".view.php")) {
            include("../views/".$file.".view.php");
        }
        else {
            exit("appcore: view: aucun fichier de ce nom '".$file.".view.php'");
        }
        
    }
    
    public function main() {
        
        if(isset($this->request)) {
            $this->classLoad($this->request);   
        }
        else {
            if(file_exists($this->path."/class/defaut.class.php")) {
                include($this->path."/class/defaut.class.php");
            }
        
            if(class_exists("defaut")) {
                $class = new defaut();
                $class->main();
            }
        }
        
    }
}