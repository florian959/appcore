<?php
/*                               
   __ _   _ __    _ __     ___    ___    _ __    ___ 
  / _` | | '_ \  | '_ \   / __|  / _ \  | '__|  / _ \
 | (_| | | |_) | | |_) | | (__  | (_) | | |    |  __/
  \__,_| | .__/  | .__/   \___|  \___/  |_|     \___|
         | |     | |                                 
         |_|     |_|                                 
*/

// APPCORE
include("core.php");

// NOUVELLE APPLICATION
$app = new application("app-test/");
$app->set("TEMPLATE", "%PATH template.php");
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title>Une appli utilisant AppCore</title>
    <meta name="description" content="">
    <link rel="stylesheet" href="/app-test/css/style.css">
  </head>
<body>
    <h1>Mon application</h1>

    <a href="http://<?php echo $app->home(); ?>">Accueil</a> <a href="http://<?php echo $app->home(); ?>/test">Page test</a>
    <br /><br />

    <?php $app->main(); ?>
</body>
</html>